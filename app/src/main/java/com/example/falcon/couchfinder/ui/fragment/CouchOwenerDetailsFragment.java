package com.example.falcon.couchfinder.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.falcon.couchfinder.R;
import com.example.falcon.couchfinder.ui.adapter.OwenerTabsAdapter;

public class CouchOwenerDetailsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_couch_owener_details,container,false);
        fragmentView.findViewById(R.id.send_request_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((AppCompatActivity)getContext()).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new CouchRequestForm()).addToBackStack(null).commit();
            }
        });

        ViewPager viewPager = fragmentView.findViewById(R.id.view_pager);
        viewPager.setAdapter(new OwenerTabsAdapter(((AppCompatActivity)getContext()).getSupportFragmentManager()));
        TabLayout tabLayout = fragmentView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        return fragmentView;
    }
}
