package com.example.falcon.couchfinder.ui.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.example.falcon.couchfinder.ui.fragment.OwenerTabs;


public class OwenerTabsAdapter extends FragmentPagerAdapter {

    public OwenerTabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return new OwenerTabs();
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "about me";
            case 1:
                return "my home";

            case 2:
                return "reference";
            case 3:
                return "photos";
        }
        return "";
    }
}
