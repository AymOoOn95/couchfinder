package com.example.falcon.couchfinder.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.falcon.couchfinder.R;

public class CouchDetailsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_couch_details,container,false);

        fragmentView.findViewById(R.id.send_request_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((AppCompatActivity)getContext()).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new CouchOwenerDetailsFragment()).addToBackStack(null).commit();
            }
        });
        return fragmentView;
    }
}
